# Fase 1: Costruire l'applicazione Angular
FROM node:16 as build
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

# Fase 2: Servire l'applicazione con Nginx
FROM nginx:alpine
COPY --from=build /app/dist/smwmapp /usr/share/nginx/html
COPY ./src/app/config/nginx.conf /etc/nginx/nginx.conf
EXPOSE 4200
CMD ["nginx", "-g", "daemon off;"]

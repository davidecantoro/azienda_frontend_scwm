import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from "@angular/router";
import {UsersComponent} from "./components/users/users.component";
import {AboutComponent} from "./components/about/about.component";
import {ContactComponent} from "./components/contact/contact.component";

import { LoginComponent } from './components/login/login.component';
import { AgencyComponent } from './components/agency/agency.component';
import { AuthGuard } from './guards/auth.guard';
import { ContainerDetailsComponent } from './components/agency/container-details/container-details.component';
import { CassonettiComponent } from './components/agency/cassonetti/cassonetti.component';
import { PercorsiComponent } from './components/agency/percorsi/percorsi.component';
import { EmployeeTableComponent } from './components/agency/employee-table/employee-table.component'; 
import { PathDetailsComponent } from './components/agency/path-details/path-details.component';
import { Employee2Component } from './components/agency/employee2/employee2.component';
import { CreazioneCassonettoComponent } from './components/agency/creazione-cassonetto/creazione-cassonetto.component';
import { ComuneComponent } from './components/comune/comune/comune.component';
import { PerformanceCittadiniComponent } from './components/comune/performance-cittadini/performance-cittadini.component';
import { TasseComponent } from './components/comune/tasse/tasse.component';
import { TasseDetailsComponent } from './components/comune/tasse-details/tasse-details.component';
import { ComuneDetailsComponent } from './components/comune/comune-details/comune-details.component';
import { NotificheComponent } from './components/comune/notifiche/notifiche.component';
import { RegisterComponent } from './components/login/register/register.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'agency', component: AgencyComponent, canActivate: [AuthGuard] },
  {path: 'agency/container-details/:id', component: ContainerDetailsComponent, canActivate: [AuthGuard]},
  { path: 'cassonetti', component: CassonettiComponent, canActivate: [AuthGuard] },
  { path: 'percorsi', component: PercorsiComponent, canActivate: [AuthGuard] },
  { path: 'employee', component: EmployeeTableComponent ,canActivate: [AuthGuard]},
  { path: 'agency/path-details', component: PathDetailsComponent, canActivate: [AuthGuard] },
  {path: 'employee2', component: Employee2Component, canActivate: [AuthGuard]},
  {path: 'creazione-cassonetto', component: CreazioneCassonettoComponent, canActivate: [AuthGuard]},
  {path :'comune', component: ComuneComponent, canActivate: [AuthGuard]},
  { path: 'performance-cittadini', component: PerformanceCittadiniComponent ,canActivate: [AuthGuard]},
  {path: 'tasse', component: TasseComponent, canActivate: [AuthGuard]},
  {path: 'tasse-details',component: TasseDetailsComponent,canActivate: [AuthGuard]},
  {path: 'comune-details',component: ComuneDetailsComponent, canActivate: [AuthGuard]},
  {path: 'notifiche',component: NotificheComponent,canActivate: [AuthGuard]},
  {path: 'register',component: RegisterComponent,}

 




];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';

import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import {FormsModule} from "@angular/forms";
import {UsersService} from "./services/users.service";
import {HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { AgencyComponent } from 'src/app/components/agency/agency.component';
import { ContainerDetailsComponent } from './components/agency/container-details/container-details.component';
import { CassonettiComponent } from './components/agency/cassonetti/cassonetti.component';
import { PercorsiComponent } from './components/agency/percorsi/percorsi.component';
import { EmployeeTableComponent } from './components/agency/employee-table/employee-table.component';
import { PathDetailsComponent } from './components/agency/path-details/path-details.component';
import { Employee2Component } from './components/agency/employee2/employee2.component';
import { CreazioneCassonettoComponent } from './components/agency/creazione-cassonetto/creazione-cassonetto.component';
import { ComuneComponent } from './components/comune/comune/comune.component';
import { PerformanceCittadiniComponent } from './components/comune/performance-cittadini/performance-cittadini.component';
import { TasseComponent } from './components/comune/tasse/tasse.component';
import { TasseDetailsComponent } from './components/comune/tasse-details/tasse-details.component';
import { ComuneDetailsComponent } from './components/comune/comune-details/comune-details.component';
import { ChartComponentComponent } from './components/chart-component/chart-component.component';
import { NotificheComponent } from './components/comune/notifiche/notifiche.component';
import { PaymentDetailsDialogComponent } from './payment-details-dialog/payment-details-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './components/login/register/register.component'; // Necessario per le animazioni








@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    AboutComponent,
    ContactComponent,
    LoginComponent,
    AgencyComponent,
    ContainerDetailsComponent,
    CassonettiComponent,
    PercorsiComponent,
    EmployeeTableComponent,
    PathDetailsComponent,
    Employee2Component,
    CreazioneCassonettoComponent,
    ComuneComponent,
    PerformanceCittadiniComponent,
    TasseComponent,
    TasseDetailsComponent,
    ComuneDetailsComponent,
    ChartComponentComponent,
    NotificheComponent,
    PaymentDetailsDialogComponent,
    RegisterComponent,
    
  
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatDialogModule,
    BrowserAnimationsModule

  
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }

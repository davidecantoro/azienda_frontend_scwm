import { Component, OnInit, OnDestroy } from '@angular/core'; 
import { Router } from '@angular/router';
import { WebsocketService } from 'src/app/services/websocket.service'; 
import { WasteContainerService } from 'src/app/services/waste-container.service';
import { AlertsService } from 'src/app/services/AlertsService.service';
import { NotificationService } from 'src/app/services/NotificationService.service'; // Aggiusta il percorso in base alla tua struttura
import { DataSharingService } from 'src/app/services/data-sharing.service'; // Aggiusta il percorso in base alla tua struttura
import { AuthService } from 'src/app/services/auth.service';
import { PerformanceService } from 'src/app/services/performance.service';

declare let L: any;

@Component({
  selector: 'app-agency',
  templateUrl: './agency.component.html',
  styleUrls: ['./agency.component.scss']
})
export class AgencyComponent implements OnInit, OnDestroy {
  public alerts: Array<{ id: string, fillingLevel: number, timestamp: string }>;
  isSidebarClosed = false;
  public searchAddress: string = '';
  private map: any; // Variabile per memorizzare l'oggetto mappa

  public containersMap: { [id: string]: any } = {}; 
  public isRouteCreationMode: boolean = false;
  public selectedContainers: string[] = [];
  public isCreatingRoute: boolean = false;
  private temporaryMarker: any; // Variabile per il marker temporaneo
  public isCreatingContainer: boolean = false;
  private containerLat: number | undefined;
  private containerLng!: number;
  private containerAddress!: string;
  public isPositionEditMode: boolean = false;
  private selectedContainerIdForEdit: string | null = null;
  private isMarkerSelectedForEdit: boolean = false;
  private boundMapClickForContainer: any;
  public userRole: string | null = null;
public userId: string | null = null;
public performanceMensili: any[] = [];
username: string | null = null; // Definisci la variabile qui
isWasteDisposalMode: boolean = false;
selectedBinId: string | null = null;
wasteDisposalAmount: number | null = null;












 








  constructor(
    private router: Router,
    private websocketService: WebsocketService,
    private wasteContainerService: WasteContainerService,
    private alertsService: AlertsService,
    private notificationService: NotificationService,
    private dataSharingService: DataSharingService,
    private authService: AuthService,
    private performanceService: PerformanceService
   
  ) {
    this.alerts = this.alertsService.alerts;
  }

  ngOnInit(): void {
    
    const jwtToken = this.authService.getRole();;
    
    if (!jwtToken) {
      console.error('JWT Token non trovato nel localStorage.');
      return;
    }
 
    console.log(localStorage.getItem('token'))
    this.userRole = this.authService.getRole(); 
    this.userId = localStorage.getItem('userId');
    this.username = localStorage.getItem('username'); // Assicurati che il nome utente sia salvato con questa chiave
    console.log(this.username)

    if (this.userRole === 'citizen' && this.userId) {
      this.loadPerformanceMensili(this.userId);
    }
    this.initMap();
    
    

    this.websocketService.connect(() => {
      this.websocketService.subscribeToTopic('/topic/alerts', (message: any) => {
        const alertDTO = JSON.parse(message.body);
        const containerId = alertDTO.binId;
        const newFillingLevel = alertDTO.fillingLevel;
        const date = alertDTO.timestamp;

        this.alertsService.addAlert({
          id: containerId,
          fillingLevel: newFillingLevel,
          timestamp: date
        });

        if (this.containersMap[containerId]) {
          let containerInfo = this.containersMap[containerId];
          containerInfo.fillingLevel = newFillingLevel;
          let updatedPopupContent = `
              
              ID: ${containerId} <br>

              Filling Level: ${newFillingLevel} <br>
              Type: ${containerInfo.type}
              
          `;

          containerInfo.marker.getPopup().setContent(updatedPopupContent);
        }
      });
    });

    
    this.initializeMarkerClickEvents();
  }

  ngOnDestroy(): void {
    
  }
  toggleSidebar(): void {
    this.isSidebarClosed = !this.isSidebarClosed;
  }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.websocketService.disconnect();
    this.router.navigate(['/login']);
  }

  focusOnMarker(containerId: string): void {
    if (this.containersMap[containerId]) {
      const container = this.containersMap[containerId];
      const marker = container.marker;
      if (marker) {
        marker._map.setView(marker.getLatLng(), 17);
        marker.openPopup();
      } else {
        console.error('Marker non trovato per il container:', containerId);
      }
    } else {
      console.error('Container non trovato con ID:', containerId);
    }
  }

  initMap(): void {
    const lecceCoordinates = [40.3505, 18.1750];
    this.map = L.map('map', {
      center: lecceCoordinates,
      zoom: 13,
      maxBounds: [
        [40.3000, 18.1000], 
        [40.4000, 18.2500]  
      ],
      maxZoom: 20,
      minZoom: 12,
      zoomControl: false
    });
  
    const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  
    this.wasteContainerService.getAllWasteContainers().subscribe(wasteContainers => {
      wasteContainers.forEach((container: { latitude: any; longitude: any; id: string; }) => {
        const marker = L.marker([container.latitude, container.longitude]).addTo(this.map);
        marker.bindPopup(`Caricamento dei dati...`);
        
        marker.on('mouseover', () => marker.openPopup());
        marker.on('mouseout', () => marker.closePopup());
        marker.on('click', () => {
          if (this.isRouteCreationMode) {
            this.selectContainer(container.id);
          }
          
        });
  
        this.containersMap[container.id] = { ...container, marker };
      });
      this.updateAllContainerFillingLevels();
  
     
    });
  
    this.map.on('click', (event: any) => {
      const lat = event.latlng.lat;
      const lng = event.latlng.lng;
      
      this.fetchAddress(lat, lng);
      

      console.log(`Latitudine: ${lat}, Longitudine: ${lng}`);
  
      
    });
  }
  
  searchAndNavigate(): void {
    if (!this.searchAddress) {
      console.error('Indirizzo non specificato.');
      return;
    }

    fetch(`https://nominatim.openstreetmap.org/search?format=json&q=${encodeURIComponent(this.searchAddress)}`)
      .then(response => response.json())
      .then(data => {
        if (data && data.length > 0) {
          const lat = data[0].lat;
          const lon = data[0].lon;
          this.map.setView([lat, lon], 17); // Sposta la mappa e imposta il livello di zoom
        } else {
          console.error('Nessun risultato trovato.');
        }
      })
      .catch(error => {
        console.error('Errore nella ricerca:', error);
      });
  }

  updateAllContainerFillingLevels(): void {
    const containerIds = Object.keys(this.containersMap);
    this.wasteContainerService.getLatestFillingLevels(containerIds).subscribe(latestDataArray => {
      latestDataArray.forEach(latestData => {
        const container = this.containersMap[latestData.idBin];
        if (container) {
          container.fillingLevel = latestData.fillingLevel; 
          if (container.fillingLevel > 0.8) {
            // Verifica se latestData.timestamp è un oggetto Date
            const timestampStr = latestData.timestamp instanceof Date 
                                 ? latestData.timestamp.toISOString() 
                                 : latestData.timestamp;
    
            this.alertsService.addAlert({
              id: container.id,
              fillingLevel: container.fillingLevel,
              timestamp: timestampStr 
            });
          }
  
          let updatedPopupContent = `ID: ${latestData.idBin} <br> Filling Level: ${container.fillingLevel} <br> Type: ${container.type}`;
          container.marker.getPopup().setContent(updatedPopupContent);
        }
      });
    });
  }


  connectWebSocket(): void {
    this.websocketService.connect(() => {
      this.websocketService.subscribeToTopic('/topic/alerts', (message: any) => {
        // Gestione dei messaggi WebSocket
      });
    });
  }

  navigateToDetails(id: string): void {
    this.router.navigate(['agency/container-details', id]);
  }
  enterRouteCreationMode(): void {
    this.isRouteCreationMode = !this.isRouteCreationMode;
  
    if (this.isRouteCreationMode) {
      this.notificationService.showToast('Modalità di selezione dei cassonetti attiva. Clicca sui cassonetti per aggiungerli al percorso.');
    } else {
      this.resetSelection();
    }
  }
  resetSelection(): void {
    this.selectedContainers = []; // Svuota l'array di container selezionati
    console.log('Selezione annullata. Container selezionati:', this.selectedContainers);
  
    // Resetta i marker al loro stato originale
    for (let key in this.containersMap) {
      const container = this.containersMap[key];
  
     
      container.marker.setIcon(L.Marker.prototype.options.icon);
    }
  
    this.isRouteCreationMode = false; // Disattiva la modalità di creazione percorso
  }
  
  selectContainer(containerId: string): void {
    if (this.isRouteCreationMode) {
      // Controlla se il cassonetto è già stato selezionato
      if (!this.selectedContainers.includes(containerId)) {
        this.selectedContainers.push(containerId);
  
        // Stampa l'array aggiornato degli ID dei container selezionati
        console.log('Container selezionati:', this.selectedContainers)
  
        
        const containerMarker = this.containersMap[containerId]?.marker;
        if (containerMarker) {
         
          const selectionNumber = this.selectedContainers.length;
          const icon = this.createNumberedIcon(selectionNumber);
          containerMarker.setIcon(icon);
  
         
          
        } else {
          console.error('Marker non trovato per il container:', containerId);
        }
      }
    }
  }
  continueWithRoute(): void {
    // Logica per continuare con la creazione del percorso
    this.dataSharingService.setSelectedContainerIds(this.selectedContainers);
    this.router.navigate(['/employee']);

    console.log('Continuando con i container selezionati:', this.selectedContainers);
   
  }
  createNumberedIcon(number: number): any {
    return L.divIcon({
      className: 'custom-div-icon', 
      html: `<div style='background-color: black; color: white; border-radius: 50%; width: 35px; height: 35px; text-align: center; line-height: 35px; font-weight: bold; font-size: 16px;'>${number}</div>`, // HTML per il numero con sfondo nero e testo bianco
      iconSize: [35, 35], // Dimensioni del divIcon
      iconAnchor: [17, 35], // Posizione dell'icona rispetto al punto del marker
    });
  }
  enterContainerCreationMode(): void {
    this.isCreatingContainer = true;
    this.boundMapClickForContainer = this.onMapClickForContainer.bind(this);
    this.map.on('click', this.boundMapClickForContainer);
  }
  cancelContainerCreation(): void {
    this.isCreatingContainer = false;
    if (this.temporaryMarker) {
      this.map.removeLayer(this.temporaryMarker);
      this.temporaryMarker = null; 
    }
    this.map.off('click', this.boundMapClickForContainer);
  }

  finalizeContainerCreation(): void {
    if (this.containerLat && this.containerLng && this.containerAddress) {
      this.router.navigate(['/creazione-cassonetto'], { 
        queryParams: { 
          lat: this.containerLat, 
          lng: this.containerLng, 
          address: this.containerAddress 
        } 
      });
    } else {
      console.error('Coordinate o indirizzo non disponibili.');
    }
    this.cancelContainerCreation();
  }

  onMapClickForContainer(event: any): void {
    const lat = event.latlng.lat;
    const lng = event.latlng.lng;
    this.containerLat = lat;
    this.containerLng = lng;

    if (this.temporaryMarker) {
      this.map.removeLayer(this.temporaryMarker);
    }
    this.temporaryMarker = L.marker([lat, lng]).addTo(this.map);
    this.fetchAddress(lat, lng);
    
    
  }
  createQuickRoute(): void {
    const containersForQuickRoute: string[] = [];
    Object.values(this.containersMap).forEach(container => {
      if (container.fillingLevel > 0.5) {
        containersForQuickRoute.push(container.id);
      }
    });

    if (containersForQuickRoute.length > 0) {
      
      console.log('Cassonetti selezionati per il percorso rapido:', containersForQuickRoute);

      this.wasteContainerService.sortWasteContainers(containersForQuickRoute).subscribe(
        sortedIds => {
          console.log('Cassonetti ordinati:', sortedIds);
          this.dataSharingService.setSelectedContainerIds(sortedIds); 
          this.router.navigate(['/employee']); 
        },
        error => {
          console.error('Errore durante l\'ordinamento:', error);
        }
      );
    } else {
      this.notificationService.showToast('Nessun cassonetto necessita di pulizia immediata.');
    }
  }
 
  
  enterPositionEditMode(): void {
    this.isPositionEditMode = true;
    this.isMarkerSelectedForEdit = false

    this.notificationService.showToast('Seleziona un cassonetto');
    
    for (const id in this.containersMap) {
      const container = this.containersMap[id];
      container.marker.off('click').on('click', () => {
        if (this.isPositionEditMode && !this.isMarkerSelectedForEdit) {
          this.selectedContainerIdForEdit = id;
          this.map.removeLayer(container.marker);
          this.map.off('click', this.onMapClickForContainer.bind(this));
          this.map.on('click', this.onMapClickForNewPosition.bind(this));
          this.isMarkerSelectedForEdit = true; // Imposta che un marker è stato selezionato

        }
      });
    }
  }
  onMapClickForNewPosition(event: any): void {
    const lat = event.latlng.lat;
    const lng = event.latlng.lng;
  
    if (this.temporaryMarker) {
      this.map.removeLayer(this.temporaryMarker);
    }
    this.temporaryMarker = L.marker([lat, lng]).addTo(this.map);
    this.containerLat = lat;
    this.containerLng = lng;
  
    this.fetchAddress(lat, lng);

    
  }
  confirmPositionUpdate(): void {
    if (this.selectedContainerIdForEdit && this.containerLat && this.containerLng && this.containerAddress) {
      const updatedData = {
        id: this.selectedContainerIdForEdit,
        latitude: this.containerLat,
        longitude: this.containerLng,
        via: this.containerAddress
      };
  
      this.wasteContainerService.updateWasteContainerLocation(updatedData).subscribe(
        response => {
          console.log('Posizione cassonetto aggiornata:', response);
          this.notificationService.showToast('Posizione cassonetto aggiornata');
        
          if (this.selectedContainerIdForEdit != null) {
            const container = this.containersMap[this.selectedContainerIdForEdit];
             this.containersMap[this.selectedContainerIdForEdit].marker = this.temporaryMarker;
             this.assignMarkerEvents(this.temporaryMarker, container);
          }

          this.temporaryMarker = null;
          this.selectedContainerIdForEdit = null;
          this.exitPositionEditMode();
        },
        error => {
          console.error('Errore nell\'aggiornamento:', error);
          this.notificationService.showToast('Errore nell\'aggiornamento');
        }
      );
    } else {
      console.error('Dati mancanti per l\'aggiornamento');
      this.notificationService.showToast('Dati mancanti per l\'aggiornamento');
    }
  }
  assignMarkerEvents(marker: any, containerData: any): void {
    marker.bindPopup(`ID: ${containerData.id} <br> Filling Level: ${containerData.fillingLevel} <br> Type: ${containerData.type}`);
    
    marker.on('mouseover', () => marker.openPopup());
    marker.on('mouseout', () => marker.closePopup());
    // Aggiungi altri eventi se necessario
  }
  
  removeMarker(markerId: string): void {
    const marker = this.containersMap[markerId]?.marker;
    if (marker) {
      this.map.removeLayer(marker);
      delete this.containersMap[markerId]; 
    }
  }
  

  exitPositionEditMode(): void {
    this.isPositionEditMode = false;
    
    this.initializeMarkerClickEvents();
  
    if (this.temporaryMarker) {
      this.map.removeLayer(this.temporaryMarker);
      this.temporaryMarker = null;
    }
  
    this.map.off('click', this.onMapClickForNewPosition.bind(this));
    this.selectedContainerIdForEdit = null;
  }
  
  initializeMarkerClickEvents(): void {
    for (const id in this.containersMap) {
      const container = this.containersMap[id];
      container.marker.off('click').on('click', () => {
        if (this.isRouteCreationMode) {
         
          this.selectContainer(id);
        } else if (this.isWasteDisposalMode) {
          this.handleWasteDisposal(container);
        }
      });
    }
  }
  
  handleWasteDisposal(container: any): void {
    const wasteDisposalInput = prompt("Inserisci la quantità di rifiuti conferiti:");
    if (wasteDisposalInput !== null && !isNaN(Number(wasteDisposalInput))) {
      const wasteDisposal = Number(wasteDisposalInput); 
      const newFillingLevel = container.fillingLevel + wasteDisposal;
  
      if (newFillingLevel > 1) {
        this.handleWasteDisposal(container); // Richiama la funzione per permettere all'utente di inserire un nuovo valore

      } else {
        const disposalData = {
          idBin: container.id,
          timestamp: new Date().toISOString(),
          fillingLevel: newFillingLevel,
          waste_disposal: wasteDisposal,
          citizen: this.userId
        };
  
        this.alertsService.reportWasteDisposal(disposalData).subscribe({
          next: (response) => {
            this.notificationService.showToast(`Il conferimento è avvenuto con successo`);
            this.exitWasteDisposalMode(); // Esce dalla modalità di conferimento solo dopo un successo
          },
          error: (error) => {
            console.error('Errore nel conferimento rifiuti:', error);
            

          }
        });
      }
    } else if (wasteDisposalInput === null) {
      this.exitWasteDisposalMode(); 
    }
   
  }
  exitWasteDisposalMode(): void {
    this.isWasteDisposalMode = false;
    
    this.initializeMarkerClickEvents(); // Ri-inizializza gli eventi di clic sui marker
  }
  
  
  fetchAddress(lat: number, lng: number): void {
    fetch(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lng}`)
      .then(response => response.json())
      .then(data => {
        this.containerAddress = data.display_name;
        this.notificationService.showToast(`Indirizzo: ${this.containerAddress}`);
      })
      .catch(error => {
        console.error('Errore nel geocoding inverso:', error);
      });
  }
  navigateToPerformanceCitizen(): void {
    if (this.userId) {
      this.router.navigate(['performance-cittadini'], { queryParams: { id: this.userId } });
    } else {
      console.error('ID utente non disponibile');
    }
  }

  viewNotifications(): void {
    this.router.navigate(['/notifiche'], { queryParams: { 'idCittadino': this.userId} });
  }
  viewHistory(): void {
    this.router.navigate(['/tasse-details'], { queryParams: { id: this.userId} });
  }
  loadPerformanceMensili(userId: string): void {
    this.performanceService.getPerformanceByCitizen(userId).subscribe(data => {
      this.performanceMensili = data.performanceAnnuale[0].performanceMensili;
     
    }, error => {
      console.error("Errore nel caricamento delle performance", error);
    });
  }
  enterWasteDisposalMode(): void {
    this.isWasteDisposalMode = true;
    this.notificationService.showToast('Seleziona un cassonetto per conferire i rifiuti.');
    this.initializeMarkerClickEvents(); // Ri-inizializza gli eventi di clic sui marker per questa modalità
  }
 
}
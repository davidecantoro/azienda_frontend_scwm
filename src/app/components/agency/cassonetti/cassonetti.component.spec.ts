import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CassonettiComponent } from './cassonetti.component';

describe('CassonettiComponent', () => {
  let component: CassonettiComponent;
  let fixture: ComponentFixture<CassonettiComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CassonettiComponent]
    });
    fixture = TestBed.createComponent(CassonettiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

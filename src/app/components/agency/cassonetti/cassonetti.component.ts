
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebsocketService } from 'src/app/services/websocket.service'; 
import { WasteContainerService } from 'src/app/services/waste-container.service';
import { AlertsService } from 'src/app/services/AlertsService.service';

@Component({
  selector: 'app-cassonetti',
  templateUrl: './cassonetti.component.html',
  styleUrls: ['./cassonetti.component.scss']
})
export class CassonettiComponent implements OnInit {
  containers: any[] = [];
  isSidebarClosed = false;

  constructor(
    private router: Router,
    private websocketService: WebsocketService,
    private wasteContainerService: WasteContainerService,
    private alertsService: AlertsService  
  ) {}

  ngOnInit() {
    this.loadContainers();
    this.subscribeToWebSocket();
  }

 
  loadContainers(): void {
    this.wasteContainerService.getAllWasteContainers().subscribe(containers => {
      this.containers = containers; 
      this.updateAllContainerFillingLevels(); 
    });
  }
  
  
  updateAllContainerFillingLevels(): void {
    const containerIds = this.containers.map(container => container.id);
    this.wasteContainerService.getLatestFillingLevels(containerIds).subscribe(latestDataArray => {
      latestDataArray.forEach(latestData => {
        const container = this.containers.find(c => c.id === latestData.idBin);
        if (container) {
          container.fillingLevel = latestData.fillingLevel;
        }
      });
      this.containers.sort((a, b) => b.fillingLevel - a.fillingLevel);

    });
  }
  

subscribeToWebSocket(): void {
  this.websocketService.subscribeToTopic('/topic/alerts', (message: any) => {
    const alertDTO = JSON.parse(message.body);
    const updatedContainer = this.containers.find(c => c.id === alertDTO.binId);
    if (updatedContainer) {
      updatedContainer.fillingLevel = alertDTO.fillingLevel;
      this.containers.sort((a, b) => b.fillingLevel - a.fillingLevel); // Riordinamento decrescente
    }
  });
}
  isAlarm(fillingLevel: number): boolean {
    return fillingLevel > 0.8;
  }
  
  navigateToDetails(id: string): void {
    this.router.navigate(['agency/container-details', id]);
  }
  
 
 

  toggleSidebar(): void {
    this.isSidebarClosed = !this.isSidebarClosed;
  }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
  
}

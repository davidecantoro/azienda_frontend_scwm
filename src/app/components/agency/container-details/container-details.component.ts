import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WasteContainerService } from 'src/app/services/waste-container.service';
import { WasteContainerModelService } from 'src/app/services/waste-containers-model.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-container-details',
  templateUrl: './container-details.component.html',
  styleUrls: ['./container-details.component.scss']
})
export class ContainerDetailsComponent implements OnInit {
  containerId!: string;
  containerDetails: any;
  modelDetails: any;

  constructor(private route: ActivatedRoute, 
              private wasteContainerService: WasteContainerService, 
              private modelService: WasteContainerModelService,
              private router: Router) { }

  ngOnInit(): void {
    // Estrai l'ID dall'URL e assegnalo a containerId
    this.containerId = this.route.snapshot.paramMap.get('id') ?? '';

    // Prima chiamata: per ottenere i dettagli del contenitore
    this.wasteContainerService.getContainerDetails(this.containerId).subscribe(data => {
      this.containerDetails = data;

      // Dopo aver ricevuto i dettagli del contenitore, verifica se è presente un modello da caricare
      if(this.containerDetails && this.containerDetails.refWasteContainerModel) {
        // Seconda chiamata: per ottenere i dettagli del modello di contenitore
        this.modelService.getModelContainerDetails(this.containerDetails.refWasteContainerModel).subscribe(modelData => {
          this.modelDetails = modelData;
        });
      }
    });
  }
  isSidebarClosed = false;

  toggleSidebar(): void {
    this.isSidebarClosed = !this.isSidebarClosed;
  }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}

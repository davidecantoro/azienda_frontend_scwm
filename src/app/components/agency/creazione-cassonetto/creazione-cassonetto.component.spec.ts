import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreazioneCassonettoComponent } from './creazione-cassonetto.component';

describe('CreazioneCassonettoComponent', () => {
  let component: CreazioneCassonettoComponent;
  let fixture: ComponentFixture<CreazioneCassonettoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreazioneCassonettoComponent]
    });
    fixture = TestBed.createComponent(CreazioneCassonettoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

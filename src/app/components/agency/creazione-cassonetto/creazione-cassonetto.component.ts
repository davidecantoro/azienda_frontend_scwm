import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WasteContainerService } from 'src/app/services/waste-container.service';
import { WasteContainerModel } from 'src/app/models/waste-container-model.interface';
import { NotificationService } from 'src/app/services/NotificationService.service';

@Component({
  selector: 'app-creazione-cassonetto',
  templateUrl: './creazione-cassonetto.component.html',
  styleUrls: ['./creazione-cassonetto.component.scss']
})
export class CreazioneCassonettoComponent {
  isSidebarClosed = false;
  wasteContainerModels: WasteContainerModel[] = [];
  selectedModelId: string | undefined;
  latitude: string | undefined;
  longitude: string | undefined;
  via: string | undefined;
  selectedWasteType: string = '';
  serialNumber: string = '';
  newContainer: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private wasteContainerService: WasteContainerService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.latitude = params['lat'];
      this.longitude = params['lng'];
      this.via = params['address'];
    });

    this.loadWasteContainerModels();
  }

  loadWasteContainerModels(): void {
    this.wasteContainerService.getAllWasteContainerModels().subscribe(models => {
      this.wasteContainerModels = models;
    });
  }

  selectModel(modelId: string): void {
    this.selectedModelId = modelId;
  }

  onContinue(): void {
    if (this.isValidContainerData()) {
      this.createWasteContainer();
      this.wasteContainerService.createWasteContainer(this.newContainer).subscribe({
        next: (response) => {
          this.notificationService.showToast('Cassonetto creato con successo');
          this.router.navigate(['agency']);

        },
        error: (err) => {
          this.notificationService.showToast('Errore nella creazione del cassonetto');
        }
      });
    } else {
      this.notificationService.showToast('Per favore compila tutti i campi');
    }
  }

  isValidContainerData(): boolean {
    return Boolean(this.selectedModelId && this.latitude && this.longitude && this.via && this.selectedWasteType && this.serialNumber);
}
  createWasteContainer(): void {
    this.newContainer = {
      refWasteContainerModel: this.selectedModelId,
      latitude: this.latitude,
      longitude: this.longitude,
      via: this.via,
      type: this.selectedWasteType,
      serialNumber: this.serialNumber
    };
  }

  navigateToDetails(id: string): void {
    this.router.navigate(['agency/container-details', id]);
  }

  toggleSidebar(): void {
    this.isSidebarClosed = !this.isSidebarClosed;
  }

  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}

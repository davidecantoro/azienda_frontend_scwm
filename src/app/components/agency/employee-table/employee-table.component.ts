import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/User.service';
import { DataSharingService } from 'src/app/services/data-sharing.service'; 
import { VehicleService } from 'src/app/services/vehicle.service';
import { PathService } from 'src/app/services/path.service';
import { NotificationService } from 'src/app/services/NotificationService.service';
import { Router } from '@angular/router'; // Importa Router



@Component({
  selector: 'app-employee-table',
  templateUrl: './employee-table.component.html',
  styleUrls: ['./employee-table.component.scss']
})
export class EmployeeTableComponent implements OnInit {
  employees: any[] = [];
  selectedContainerIds: string[] = [];
  selectedDriverId: string = '';
  selectedDate: string = '';
  vehicles: any[] = [];
  selectedVehicleId: string = '';
  statoPercorso: string = '' 
  isSidebarClosed = false;
  currentPage: number = 0;
  size: number = 5;
  




  constructor(
    
    private dataSharingService: DataSharingService,
    private vehicleService: VehicleService,
    private pathService: PathService,
    private notificationService: NotificationService,
    private router: Router, // Inietta il Router,
    private userService: UserService


    ) {}

    loadMore(): void {
      
      this.userService.getByRole('employee', this.currentPage, this.size).subscribe({
        next: (data) => {
          this.employees = [...this.employees, ...data.content]; 
          this.currentPage++;
        },
        error: (err) => console.error(err),
      });
    }
    

    

  ngOnInit(): void {
    this.loadMore(); // Carica i primi dati all'inizio
    this.selectedContainerIds = this.dataSharingService.getSelectedContainerIds();
    this.vehicleService.getVehiclesOffRoute().subscribe(
      data => {
        this.vehicles = data;
      },
      error => {
        console.error('Errore durante il recupero dei veicoli:', error);
      }
    );
  }



  
  
  selectEmployee(employeeId: string): void {
    this.selectedDriverId = employeeId;
    
  }
  selectVehicle(vehicleId: string): void { 
    this.selectedVehicleId = vehicleId;
    ;
  }
  onContinueClick(): void {
    if (this.selectedDriverId && this.selectedVehicleId && this.selectedDate && this.selectedContainerIds.length > 0) {
      const routeObject = {
        route: this.selectedContainerIds.map(id => `${id}`),
        driver: this.selectedDriverId,
        day: this.selectedDate,
        vehicle: this.selectedVehicleId,
        statoPercorso: 'Non completato'
      };

      this.pathService.createPath(routeObject).subscribe(
        response => {
          console.log('Percorso inviato con successo', response);
          this.notificationService.showToast('Percorso creato con successo');
          this.router.navigate(['/agency']); // Naviga al componente 'agency'
        },
        error => {
          
          this.notificationService.showToast('Errore nell\'invio del percorso');
        }
      );
    } else {
      this.notificationService.showToast('Informazioni incomplete per creare il percorso');
    }
  }

  onDateSelect(event: any): void {
    this.selectedDate = event.target.value;
  }
  
  navigateToDetails(id: string): void {
    this.router.navigate(['agency/container-details', id]);
  }
  
 
 

  toggleSidebar(): void {
    this.isSidebarClosed = !this.isSidebarClosed;
  }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

}



  


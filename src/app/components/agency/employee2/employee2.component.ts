// employee2.component.ts
import { Component, OnInit } from '@angular/core';
import { PathService } from 'src/app/services/path.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-employee2',
  templateUrl: './employee2.component.html',
  styleUrls: ['./employee2.component.scss']
})
export class Employee2Component implements OnInit {
  username: string | null = '';
  paths: any[] = []; 
  userId: string | null = '';  
  isSidebarClosed = false;


  constructor(private pathService: PathService,private router: Router) {}

  ngOnInit() {
   this.username = localStorage.getItem('username');
   this.userId = localStorage.getItem('userId')
   this.getPaths();
   ; 

  }
  getPaths() {
    // Utilizza l'ID dell'utente per ottenere i percorsi
    if (this.userId) {
      this.pathService.getPathsByDriverId(this.userId).subscribe(
        data => {
          this.paths = data;
        },
        error => {
          console.error('Error fetching paths', error);
        }
      );
    }
  }
  viewDetails(path: any): void {
    this.router.navigate(['agency/path-details', {
      id: path.id,
      route: path.route,
      vehicle: path.vehicle,
      driver: path.driver
    }]);
  }

  navigateToDetails(id: string): void {
    this.router.navigate(['agency/container-details', id]);
  }
  
 
 

  toggleSidebar(): void {
    this.isSidebarClosed = !this.isSidebarClosed;
  }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}

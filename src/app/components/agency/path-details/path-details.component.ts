import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { WasteContainerService } from 'src/app/services/waste-container.service';
import { NotificationService } from 'src/app/services/NotificationService.service';
import { AlertsService } from 'src/app/services/AlertsService.service';


import { VehicleService } from 'src/app/services/vehicle.service';
import { UserService } from 'src/app/services/User.service';
import { PathService } from 'src/app/services/path.service';


@Component({
  selector: 'app-path-details',
  templateUrl: './path-details.component.html',
  styleUrls: ['./path-details.component.scss']
})
export class PathDetailsComponent implements OnInit {
  route: string[] = []; // Inizializza come array vuoto
  vehicle: string = ''; // Inizializza come stringa vuota
  driver: string = ''; // Inizializza come stringa vuota
  id: string = ''
  containers: any;
  vehicleDetails: any;
  driverDetails: any;
  userRole: string | null = ''; 
  cleanedContainers: Set<string> = new Set(); // Memorizza gli ID dei cassonetti puliti



 


  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private wasteContainerService: WasteContainerService,
    private vehicleService: VehicleService,
    private userService: UserService,
    private pathService: PathService,
    private notificationService: NotificationService,
    private alertService: AlertsService
    
    
    ) { }

    isSidebarClosed = false;
    ngOnInit(): void {
      this.userRole = localStorage.getItem('role');

      this.activatedRoute.params.subscribe(params => {
        if (params['id']) {
          this.id = params['id'];
          console.log('id:', this.id);
        }
    
        if (params['route']) {
          this.route = params['route'].split(',');
          console.log('Route:', this.route);
  
          
          this.wasteContainerService.getContainersByIds(this.route).subscribe(
            data => {
              this.containers = data;
              console.log('Containers:', this.containers);
              if (this.userRole === 'employee') {
                this.checkContainersFillingLevel();
              }
            },
            error => {
              console.error('Errore nel recupero dei dettagli dei cassonetti:', error);
            }
          );
        }
        if (params['vehicle']) {
          this.vehicle = params['vehicle'];
          console.log('Vehicle:', this.vehicle);
        }
        if (params['driver']) {
          this.driver = params['driver'];
          console.log('Driver:', this.driver);
        }
        
      });
      if (this.vehicle) {
        this.vehicleService.getVehicleById(this.vehicle).subscribe(
          data => {
            this.vehicleDetails = data;
            console.log('Vehicle Details:', this.vehicleDetails);
          },
          error => {
            console.error('Errore nel recupero dei dettagli del veicolo:', error);
          }
        );
      }
      if (this.driver) {
        this.userService.getUserById(this.driver).subscribe(
          data => {
            this.driverDetails = data;
            console.log('driver Details:', this.driverDetails);
          },
          error => {
            console.error('Errore nel recupero dei dettagli del veicolo:', error);
          }
        );
      }
      

      
      
    
    }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }
  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
  deletePathById(pathId: string): void {
    if (pathId) {
      this.pathService.deletePathById(pathId).subscribe(
        () => {
          // La chiamata ha avuto successo, quindi mostra la notifica di successo
          this.notificationService.showToast('Percorso eliminato con successo');
    
          // Reindirizza al componente 'Percorsi'
          this.router.navigate(['/percorsi']);
        },
        error => {
         
          this.notificationService.showToast('Errore nell\'eliminazione del percorso');
        }
      );
    }
  }
  cleanContainer(containerId: string): void {
    this.alertService.cleanContainer(containerId).subscribe(
      () => {
        this.cleanedContainers.add(containerId); // Aggiungi l'ID alla lista dei cassonetti puliti
        this.notificationService.showToast('Cassonetto pulito');
      },
      error => {
        // Gestisci l'errore qui
        console.error('Errore durante la pulizia del cassonetto:', error);
        this.notificationService.showToast('Cassonetto non pulito');
      }
    );
  }
  
  checkContainersFillingLevel(): void {
    const containerIds = this.containers.map((container: { id: any; }) => container.id);
  
    this.wasteContainerService.getLatestFillingLevels(containerIds).subscribe(
      fillingLevels => {
        fillingLevels.forEach(fillingLevel => {
          if (fillingLevel.fillingLevel === 0.0) {
            this.cleanedContainers.add(fillingLevel.idBin);
          }
        });
      },
      error => console.error('Errore nel controllo dei livelli di riempimento:', error)
    );
  }

  closePath(): void {
    if (this.id && this.vehicle) { 
      this.pathService.updatePathStatus(this.id, 'completato').subscribe(
        () => {
          
          
          // Dopo aver chiuso con successo il percorso, aggiorna lo stato del veicolo
          this.vehicleService.updateVehicleStatus(this.vehicle, 'offRoute').subscribe(
            () => {
              this.notificationService.showToast('Percorso chiuso con successo');
              this.router.navigate(['/employee2']); // Ridireziona al componente /employee2
          
            },
            error => {
              console.error("Errore durante l 'aggiornamento dello stato del veicolo:", error);
              this.notificationService.showToast("Errore durante l' aggiornamento dello stato del veicolo");
            }
          );
        },
        error => {
          console.error('Errore durante la chiusura del percorso:', error);
          this.notificationService.showToast('Errore durante la chiusura del percorso');
        }
      );
    } else {
      
      this.notificationService.showToast('ID del percorso o del veicolo non disponibile');
    }
  }
}

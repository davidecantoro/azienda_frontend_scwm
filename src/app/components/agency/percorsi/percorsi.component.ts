import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PathService } from 'src/app/services/path.service';
import { map, forkJoin, of } from 'rxjs'; 
import { UsersService } from 'src/app/services/users.service';


@Component({
  selector: 'app-percorsi',
  templateUrl: './percorsi.component.html',
  styleUrls: ['./percorsi.component.scss']
})
export class PercorsiComponent {
  paths: any[] = [];
  percorsiCompletati: any[] = [];
  percorsiNonCompletati: any[] = [];

  constructor(
    private router: Router,
    private pathService: PathService,
    private usersService: UsersService 

  
  ) {}
  ngOnInit(): void {
    this.pathService.getAllPaths().subscribe(
      data => {
        this.percorsiCompletati = data.filter((path: { statoPercorso: string; }) => path.statoPercorso === 'completato');
        this.percorsiNonCompletati = data.filter((path: { statoPercorso: string; }) => path.statoPercorso === 'Non completato');
      },
      error => {
        console.error('Errore durante il recupero dei percorsi:', error);
      }
    );
  }




  isSidebarClosed = false;

  navigateToDetails(path: any): void {
    this.router.navigate(['agency/path-details', {
      id: path.id,
      route: path.route,
      vehicle: path.vehicle,
      driver: path.driver
    }]);
  }
  
 

  toggleSidebar(): void {
    this.isSidebarClosed = !this.isSidebarClosed;
  }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
  
}




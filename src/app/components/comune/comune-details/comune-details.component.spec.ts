import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComuneDetailsComponent } from './comune-details.component';

describe('ComuneDetailsComponent', () => {
  let component: ComuneDetailsComponent;
  let fixture: ComponentFixture<ComuneDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ComuneDetailsComponent]
    });
    fixture = TestBed.createComponent(ComuneDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/User.service';
import { PerformanceService } from 'src/app/services/performance.service';
import { NotificationService } from 'src/app/services/NotificationService.service';
import {
  Chart,
  ChartConfiguration,
  LineController,
  LineElement,
  PointElement,
  LinearScale,
  CategoryScale,
  Title,
  Tooltip,
  Filler,
  Legend
} from 'chart.js';

Chart.register(
  LineController,
  LineElement,
  PointElement,
  LinearScale, // Assicurati che questo sia incluso
  CategoryScale,
  Title,
  Tooltip,
  Filler,
  Legend
);



@Component({
  selector: 'app-comune',
  templateUrl: './comune.component.html',
  styleUrls: ['./comune.component.scss']
})
export class ComuneComponent {

  citizens: any[] = [];

  selectedYear: number = new Date().getFullYear(); // Anno corrente come default
  selectedMonth: number = new Date().getMonth() + 1; // Me
  isSidebarClosed = false;
  currentPage: number = 0;
  size: number = 3;
  // Aggiungi questa variabile nel tuo componente
showLoadMoreButton: boolean = true;


  constructor(

    private router: Router,
    private userService: UserService,
    private performanceService: PerformanceService,
    private notificationService: NotificationService,
  
    
    
    ) { }
    
    ngOnInit(): void {
      this.loadMore(); 

    }


    loadMore(): void {
      this.userService.getByRole('citizen', this.currentPage, this.size).subscribe({
        next: (data) => {
          // Aggiungi i nuovi cittadini all'array esistente
          const newCitizens = data.content;
          this.citizens = [...this.citizens, ...newCitizens];
    
          // Estrai gli ID dei nuovi cittadini caricati
          const newIdCittadini = newCitizens.map((citizen: { id: any; }) => citizen.id);
    
          // Chiama getPerformancesForCitizens solo per i nuovi ID
          if (newIdCittadini.length > 0) {
            this.getPerformancesForCitizens(newIdCittadini);
          }
    
          // Incrementa la pagina corrente se ci sono stati dati ricevuti
          if (newCitizens.length > 0) {
            this.currentPage++;
          }
    
          // Controlla se mostrare o meno il pulsante "Carica più"
          this.showLoadMoreButton = newCitizens.length >= this.size;
        },
        error: (err) => {
          console.error(err);
          this.showLoadMoreButton = false; // Nasconde il pulsante in caso di errore
        },
      });
    }
    getPerformancesForCitizens(idCittadini: string[]): void {
      this.performanceService.getLatestPerformanceForCitizens(idCittadini).subscribe({
        next: (performances) => {
          this.citizens = this.citizens.map(citizen => {
            const performance = performances.find((p: { idCittadino: any; }) => p.idCittadino === citizen.id);
            if (performance) {
              citizen.latestPerformance = performance.performanceAnnuale[0];
            }
            return citizen;
          });
        },
        error: (err) => console.error('Errore nel recuperare le performance', err),
      });
    }


    viewHistory(citizenId: string, year: number): void {
      this.router.navigate(['/performance-cittadini'], { queryParams: { id: citizenId} });
    }
    toggleSidebar(): void {
      this.isSidebarClosed = !this.isSidebarClosed;
    }

  navigateTo(route: string): void {
    this.router.navigate([route]);
  }
  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
  creaNotifiche(): void {
    this.performanceService.creaNotifiche().subscribe({
      next: (response) => {
        // Qui gestisci la risposta positiva
        this.notificationService.showToast('Notifiche inviate con successo');
      },
      error: (err) => {
        console.error('Errore durante l\'invio delle notifiche', err);
        // Qui gestisci eventuali errori
        this.notificationService.showToast('Errore durante l\'invio delle notifiche');
      }
    });
  }
  viewNotifications(citizenId: string): void {
    this.router.navigate(['/notifiche'], { queryParams: { idCittadino: citizenId } });
  }
  calculatePerformance(): void {
    // Prepara i dati per la richiesta come hai fatto prima
    const startDate = new Date(this.selectedYear, this.selectedMonth - 1, 1);
    const endDate = new Date(this.selectedYear, this.selectedMonth, 0);
    const startDateStr = `${startDate.getFullYear()}-${(startDate.getMonth() + 1).toString().padStart(2, '0')}-${startDate.getDate().toString().padStart(2, '0')}`;
    const endDateStr = `${endDate.getFullYear()}-${(endDate.getMonth() + 1).toString().padStart(2, '0')}-${endDate.getDate().toString().padStart(2, '0')}`;
    const requestPayload = { startDate: startDateStr, endDate: endDateStr };
  
    this.performanceService.calculatePerformance(requestPayload).subscribe({
      next: (response) => {
        // Aggiorna l'array citizens con i nuovi dati ricevuti
        response.forEach((performanceResult: { idCittadino: any; indicePerformance: any; mese: any; anno: any; }) => {
          const citizenIndex = this.citizens.findIndex(citizen => citizen.id === performanceResult.idCittadino);
          if (citizenIndex !== -1) {
            this.citizens[citizenIndex].latestPerformance = {
              performanceMensili: [{
                indicePerformance: performanceResult.indicePerformance,
                mese: performanceResult.mese
              }],
              anno: performanceResult.anno
            };
          }
        });
  
        // Stampa la notifica
        this.notificationService.showToast('Indice di performance calcolato correttamente');
      },
      error: (error) => {
        console.error('Errore durante il calcolo della performance:', error);
        this.notificationService.showToast('Errore durante il calcolo della performance');
      }
    });
  }
}




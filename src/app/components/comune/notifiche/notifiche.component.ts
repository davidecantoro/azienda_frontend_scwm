import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/User.service';
import { PerformanceService } from 'src/app/services/performance.service';
import { NotificationService } from 'src/app/services/NotificationService.service';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-notifiche',
  templateUrl: './notifiche.component.html',
  styleUrls: ['./notifiche.component.scss']
})
export class NotificheComponent implements OnInit {
  notifiche: any[] = [];
  public userRole: string | null = null;
  public userId: string | null = null;
  

  isSidebarClosed = false;
  constructor(
    private route: ActivatedRoute,

    private router: Router,

    private performanceService: PerformanceService,
    private authService: AuthService
   
  ) {}

  ngOnInit(): void {
    this.userRole = this.authService.getRole(); 
    this.userId = localStorage.getItem('userId');
    this.route.queryParams.subscribe(params => {
      const idCittadino = params['idCittadino'];
      this.loadNotifiche(idCittadino);
    });
  }

  loadNotifiche(idCittadino: string): void {
    this.performanceService.getNotificheByCittadino(idCittadino).subscribe({
      next: (notifiche: any[]) => this.notifiche = notifiche.reverse(),
      error: (err: any) => console.error('Errore durante il caricamento delle notifiche', err)
    });
  }
  
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }
  navigateToPerformanceCitizen(): void {
    if (this.userId) {
      this.router.navigate(['performance-cittadini'], { queryParams: { id: this.userId } });
    } else {
      console.error('ID utente non disponibile');
    }
  }

  viewNotifications(): void {
    this.router.navigate(['/notifiche'], { queryParams: { 'idCittadino': this.userId} });
  }
  viewHistory(): void {
    this.router.navigate(['/tasse-details'], { queryParams: { id: this.userId} });
  }
  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}

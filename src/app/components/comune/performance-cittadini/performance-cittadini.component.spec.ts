import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerformanceCittadiniComponent } from './performance-cittadini.component';

describe('PerformanceCittadiniComponent', () => {
  let component: PerformanceCittadiniComponent;
  let fixture: ComponentFixture<PerformanceCittadiniComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PerformanceCittadiniComponent]
    });
    fixture = TestBed.createComponent(PerformanceCittadiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

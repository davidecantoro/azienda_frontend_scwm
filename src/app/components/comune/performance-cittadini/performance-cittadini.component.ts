import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PerformanceService } from 'src/app/services/performance.service';
import {
  Chart,
  ChartConfiguration,
  ScriptableContext,
  TooltipItem
} from 'chart.js';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


Chart.register(
  
);

@Component({
  selector: 'app-performance-cittadini',
  templateUrl: './performance-cittadini.component.html',
  styleUrls: ['./performance-cittadini.component.scss']
})
export class PerformanceCittadiniComponent implements OnInit, AfterViewInit {
  @ViewChild('performanceChart') performanceChart!: ElementRef<HTMLCanvasElement>;
  chart!: Chart;
  id: string | undefined;
  anno: number | undefined;
  performanceMensili: any[] = [];
  isSidebarClosed = false;
  public userRole: string | null = null;
  public userId: string | null = null;

  constructor(
    private route: ActivatedRoute,
    private performanceService: PerformanceService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.userRole = this.authService.getRole(); 
    this.userId = localStorage.getItem('userId');
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      
      if (this.id) {
        this.loadPerformance(this.id);
      } else {
        this.loadLastYearPerformance();
      }
    });
  }

  ngAfterViewInit() {
    const context = this.performanceChart.nativeElement.getContext('2d');
    if (context) {
      this.initializeChart(context);
    }
  }

  loadPerformance(idCittadino: string) {
    this.performanceService.getPerformanceByCitizen(idCittadino).subscribe(
      data => {
       
        this.normalizePerformanceData(data);
      },
      error => console.error("Errore nel caricamento delle performance", error)
    );
  }
  
  private normalizePerformanceData(performanceData: any): void {
    // Controlla se l'oggetto ha la proprietà 'performanceAnnuale' e se contiene elementi
    if (performanceData.performanceAnnuale && performanceData.performanceAnnuale.length > 0) {
      const performanceAnnuale = performanceData.performanceAnnuale[0];
      this.anno = performanceAnnuale.anno;
      this.performanceMensili = performanceAnnuale.performanceMensili;
    } else {
  
      this.performanceMensili = [];
      this.anno = undefined;
    }
    this.updateChart();
  }
  

  loadLastYearPerformance() {
    this.performanceService.getLastYearPerformanceCityHall().subscribe(
      data => {
        this.normalizePerformanceData(data);
      },
      error => console.error("Errore nel caricamento delle performance dell'ultimo anno", error)
    );
  }

 

  initializeChart(context: CanvasRenderingContext2D) {
    const config: ChartConfiguration = {
      type: 'line',
      data: {
        labels: this.performanceMensili.map(perf => perf.mese),
        datasets: [{
          label: 'Indice di Performance',
          data: this.performanceMensili.map(perf => perf.indicePerformance),
          borderColor: 'black',
          backgroundColor: function(context: ScriptableContext<'line'>) {
            let value = context.dataset.data[context.dataIndex] as number;
            return value > 60 ? 'green' : 'red';
          },
          pointRadius: 10,
          pointHoverRadius: 15,
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        },
        plugins: {
          legend: {
            display: false
          },
          tooltip: {
            mode: 'index',
            intersect: false
          }
        }
      }
    };

    this.chart = new Chart(context, config);
  }

  updateChart() {
    if (this.chart) {
      this.chart.data.labels = this.performanceMensili.map(perf => perf.mese);
      this.chart.data.datasets.forEach(dataset => {
        dataset.data = this.performanceMensili.map(perf => perf.indicePerformance);
      });
      this.chart.update();
    }
  }
  navigateTo(route: string): void {
    this.router.navigate([route]);
  }
  navigateToPerformanceCitizen(): void {
    if (this.userId) {
      this.router.navigate(['performance-cittadini'], { queryParams: { id: this.userId } });
    } else {
      console.error('ID utente non disponibile');
    }
  }
  

  viewNotifications(): void {
    this.router.navigate(['/notifiche'], { queryParams: { 'idCittadino': this.userId} });
  }

  

  viewHistory(): void {
    this.router.navigate(['/tasse-details'], { queryParams: { id: this.userId} });
  }
  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}

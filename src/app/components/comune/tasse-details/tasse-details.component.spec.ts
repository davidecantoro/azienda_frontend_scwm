import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TasseDetailsComponent } from './tasse-details.component';

describe('TasseDetailsComponent', () => {
  let component: TasseDetailsComponent;
  let fixture: ComponentFixture<TasseDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TasseDetailsComponent]
    });
    fixture = TestBed.createComponent(TasseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/User.service';
import { Router } from '@angular/router';
import { TaxService } from 'src/app/services/tax.service';
import { NotificationService } from 'src/app/services/NotificationService.service';
import { ActivatedRoute } from '@angular/router';
import { PaymentDetailsDialogComponent } from 'src/app/payment-details-dialog/payment-details-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-tasse-details',
  templateUrl: './tasse-details.component.html',
  styleUrls: ['./tasse-details.component.scss']
})
export class TasseDetailsComponent {

  isSidebarClosed = false;
  taxesPaid: any[] = [];
  taxesUnpaid: any[] = [];
  citizenId: string | null = null;
  isShowingPaidTaxes: boolean = true; 
  selectedPayment: any = {};
  public userRole: string | null = null;
  public userId: string | null = null;
  

  constructor(
 
    private router: Router,
    private taxService: TaxService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private authService: AuthService




  
  ) { }

  ngOnInit(): void {
    this.userRole = this.authService.getRole(); 
    this.userId = localStorage.getItem('userId');
    this.citizenId = this.route.snapshot.queryParamMap.get('id');
    if (this.citizenId) {
      this.taxService.getTaxesByCitizen(this.citizenId).subscribe(taxes => {
        this.taxesPaid = taxes.filter(tax => tax.status === 'pagato');
        this.taxesUnpaid = taxes.filter(tax => tax.status === 'non pagato');
      });
    }
  }
  

  showPaymentDetails(taxId: string): void {
    this.taxService.getPaymentDetailsByTaxId(taxId).subscribe(paymentDetails => {
      this.dialog.open(PaymentDetailsDialogComponent, {
        width: '250px',
        data: paymentDetails 
      });
    }, error => {
      console.error('Errore nel recupero dei dettagli del pagamento', error);
    });
  }


  navigateTo(route: string): void {
    this.router.navigate([route]);
  }
  navigateToPerformanceCitizen(): void {
    if (this.userId) {
      this.router.navigate(['performance-cittadini'], { queryParams: { id: this.userId } });
    } else {
      console.error('ID utente non disponibile');
    }
  }
  

  viewNotifications(): void {
    this.router.navigate(['/notifiche'], { queryParams: { 'idCittadino': this.userId} });
  }

  

  viewHistory(): void {
    this.router.navigate(['/tasse-details'], { queryParams: { id: this.userId} });
  }
  makePayment(tax: any): void {
    const paymentDetails = {
      taxId: tax.id,
      amountPaid: tax.amount,
      paymentMethod: 'credit card'
    };
  
    this.taxService.makePayment(paymentDetails).subscribe({
      next: (response) => {
     
        this.ngOnInit(); 
       
      },
      error: (error) => {
       
        console.error('Errore nel pagamento della tassa', error);
      }
    });
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }


}

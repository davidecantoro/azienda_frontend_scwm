import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/User.service';
import { Router } from '@angular/router';
import { TaxService } from 'src/app/services/tax.service';
import { NotificationService } from 'src/app/services/NotificationService.service';

@Component({
  selector: 'app-tasse',
  templateUrl: './tasse.component.html',
  styleUrls: ['./tasse.component.scss']
})
export class TasseComponent implements OnInit {
  citizens: any[] = [];
  currentPage: number = 0;
  selectedYear: number | undefined;
  isSidebarClosed = false;
  size: number = 3;
  showLoadMoreButton: boolean = true;

 
  constructor(
    private userService: UserService,
    private router: Router,
    private taxService: TaxService, 
    private notificationService: NotificationService 
  ) { }

  ngOnInit(): void {
    this.loadMore(); 
  }

  loadMore(): void {
    this.userService.getByRole('citizen', this.currentPage, this.size).subscribe({
      next: (data) => {
        const newCitizens = data.content;
        this.citizens = [...this.citizens, ...newCitizens];
        const newIdCittadini = newCitizens.map((citizen: { id: any; }) => citizen.id);

        if (newCitizens.length > 0) {
          this.currentPage++;
        }

        this.showLoadMoreButton = newCitizens.length >= this.size;
      },
      error: (err) => {
        console.error(err);
        this.showLoadMoreButton = false; // Nasconde il pulsante in caso di errore
      },
    });
  }

  navigateTo(route: string): void {
    this.router.navigate([route]);
  }

  onLogout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  viewHistory(citizenId: string): void {
    this.router.navigate(['/tasse-details'], { queryParams: { id: citizenId } });
  }

  getCurrentYear(): number {
    return new Date().getFullYear();
  }
  calculateTaxes(): void {
    if (this.selectedYear) {
    

      this.taxService.calculateTaxes(this.selectedYear).subscribe({
        next: () => {
          this.notificationService.showToast('Tasse calcolate con successo');
        },
        error: (err) => {
          console.error('Errore nel calcolo delle tasse:', err);
          this.notificationService.showToast('Errore nel calcolo delle tasse');
        }
      });
    } else {
      this.notificationService.showToast('Seleziona un anno per calcolare le tasse');
    }
  }
}

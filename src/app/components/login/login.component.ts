import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/NotificationService.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'] 
})
export class LoginComponent {
  username!: string;
  password!: string;
  token!: string;
  constructor(private authService: AuthService, private router: Router, private notificationService: NotificationService,) { }

  onLogin() {
    this.authService.login(this.username, this.password).subscribe(
      success => {
        localStorage.setItem('username',this.username);
        const role = this.authService.getRole();
        switch (role) {
          case 'agency':
          case 'admin':
          case 'citizen':  
            this.router.navigate(['/agency']);
            break;
          case 'employee':
            this.router.navigate(['/employee2']); 
            break;
          case 'comune':
            this.router.navigate(['/performance-cittadini']); 
            break;
          
          default:
            console.error('Unknown role');
        }
      },
      error => {
        console.error('Login failed', error);
        this.notificationService.showToast(error);
      }
    );
  }
  login() {
    this.authService.login(this.username, this.password).subscribe(
      response => {
        this.token = response.jwt;  
        localStorage.setItem('username', this.username); // Salva il nome utente in localStorage

        
    },
      error => {
        console.error('Errore durante il login:', error);
      }
    );
  }
  navigateToRegister() {
    this.router.navigate(['/register']); 
  }
  
}

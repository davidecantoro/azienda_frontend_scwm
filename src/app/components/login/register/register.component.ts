import { Component } from '@angular/core';
import { UserService } from 'src/app/services/User.service';
import { NotificationService } from 'src/app/services/NotificationService.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  user = {
    nome: '',
    cognome: '',
    username: '',
    password: '',
    birthdate: '',
    email: ''
  };
  errorMessage: string = '';

  constructor(private userService: UserService, private router: Router, private notificationService: NotificationService) {}

  onRegister() {
    this.userService.register(this.user).subscribe({
      next: (response) => {
        // Se la registrazione va a buon fine, reindirizza al componente di login
        this.router.navigate(['/login']);
        this.notificationService.showToast("Registrazione avvenuta con successo")
      },
      error: (error) => {
        // Controlla il codice di stato per identificare se l'username è già in uso
        if (error.status === 409) {
          this.errorMessage = "Username già esistente";
          this.notificationService.showToast("Username già esistente")
        } else {
          // Gestisci altri tipi di errori o mostra un messaggio generico
          this.errorMessage = "Errore durante la registrazione";
        }
      }
    });
  }
}

import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../../models/user";
import {UsersService} from "../../services/users.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  @ViewChild('userForm') form:any;

  nome:string = "";
  cognome:string = "";

  user:User = {} as User;
  users:User[] = [];

  showDetails:boolean = true;

  isDisabled:boolean = true;

  currentClass:{} = {}

  constructor(private usersService:UsersService, private route:Router) {
  }

  ngOnInit(): void {
    console.log("Sono nel ngOnInit");
    this.nome = "Roberto";
    this.cognome = "Vergallo";

    this.user = {
      nome: "Roberto",
      cognome: "Vergallo",
      email: "roberto.vergallo@unisalento.it",
      birthdate: new Date("1983/06/13")
    }

   

    this.currentClass = {
      "btn-primary":!this.isDisabled,
      "big-text":!this.isDisabled
    }
  }

  ngAfterViewInit(): void {
    console.log("Sono nel ngAfterViewInit")
  }

  fireEvent($event: MouseEvent) {
    console.log(event)
    console.log("Cliccato")
    this.showDetails = !this.showDetails;
  }

  addNewUser() {
    //this.users.push(this.user);
  }

  onSubmit(userForm: any) {
    console.log(userForm)

    this.user.nome = userForm.form.value.nome;
    this.user.cognome = userForm.form.value.cognome;

    this.usersService.createUser(this.user);
  }

  goToDetail(id: string | undefined) {
    //....
    this.route.navigateByUrl('/about/' + id);
  }
}

const URL_USERS = 'https://dfaxe8nxs4.execute-api.us-east-1.amazonaws.com/dev/users';

// Agency
const URL_AGENCY = 'https://64xqhz017j.execute-api.us-east-1.amazonaws.com/dev';
const URL_WASTEBIN = 'https://isgjf4a2cc.execute-api.us-east-1.amazonaws.com/dev';
const URL_PATH = 'https://4rtv3lue25.execute-api.us-east-1.amazonaws.com/dev';
const URL_NOTIFICATIONS = 'https://blajiipwx7.execute-api.us-east-1.amazonaws.com/dev';
const URL_TAX ='https://nq4jq036xf.execute-api.us-east-1.amazonaws.com/dev';
const URL_PERFORMANCE='https://tyr4s160tg.execute-api.us-east-1.amazonaws.com/dev';


  export const WEBSOCKET = {
    websocket: 'http://52.5.5.198:8090/websocket'
    
  };


// Configurazione per le API relative agli allarmi (Alerts)
export const ALERT_API_CONFIG = {
    baseUrl: `${URL_AGENCY}/measurements/`, // URL base per le API degli allarmi, invio json misura 
    
  };
  
  // Configurazione per le API di autenticazione (Auth)
  export const AUTH_API_CONFIG = {
    loginUrl: `${URL_USERS}/authenticate`, // URL per l'endpoint di login
    //loginUrl: 'https://dfaxe8nxs4.execute-api.us-east-1.amazonaws.com/dev/users/authenticate', // URL per l'endpoint di login
   
  };

  export const EMPLOYEE_API_CONFIG = {
    roleUrl: `${URL_USERS}/by-role?role=employee`, 
    
  };

  export const PATH_API_CONFIG = {
    pathUrl: `${URL_PATH}/paths/`, 
    
    getPathsBydriver: `${URL_PATH}/paths/driver/{driverId}`,
    
    deletePathById: `${URL_PATH}/paths/{pathId}`,

    updatePathStatusUrl: `${URL_PATH}/paths/{pathId}/updateStatus` // URL per aggiornare lo stato del percorso

    
  };

  export const USER = {
    userById: `${URL_USERS}/{id}`, 
    registrazione: `${URL_USERS}/register-citizen`,
    roleUrl: `${URL_USERS}/by-role?role=`, 
    
  };

  export const VEHICLE = {
    getVehiclesOffRoute: `${URL_PATH}/vehicles/status/offRoute`, 
    getVehiclesById: `${URL_PATH}/vehicles/{id}`,
    updateVehicleStatus: `${URL_PATH}/vehicles/{id}/updateStatus`,
    
    
  };

  export const WASTCONTAINER = {
    apiUrl: `${URL_WASTEBIN}/waste-containers/`,
    getContainerDetails: `${URL_WASTEBIN}/waste-containers/{id}`,
    getLatestFillingLevels: `${URL_AGENCY}/measurements/latest/batch`,
    getContainerByIds:`${URL_WASTEBIN}/waste-containers/batch`,
    sortWasteContainers:`${URL_WASTEBIN}/waste-containers/sort`,
    getModels: `${URL_WASTEBIN}/waste-container-models/{id}`,
    updateWasteContainerLocation: `${URL_WASTEBIN}/waste-containers/update`,
    getModelById: `${URL_WASTEBIN}/waste-container-models/{id}`

    
  };



  export const PERFORMANCE = {
    performance: `${URL_PERFORMANCE}`

  
  };

  export const NOTIFICHE = {
    notifiche: `${URL_NOTIFICATIONS}`

  
  };


 export const TAX = {
    tax: `${URL_TAX}`

  
  };
  

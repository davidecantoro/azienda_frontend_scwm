export interface User {
  id?: string
  nome: string
  cognome: string
  email: string
  birthdate: Date
  address?: string
}

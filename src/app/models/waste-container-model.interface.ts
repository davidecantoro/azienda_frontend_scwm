export interface WasteContainerModel {
    id: string;
    modelName: string;
    brandName: string;
    madeOf: string
  }
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-payment-details-dialog',
  template: `
    <div class="custom-dialog">
      <h2>Dettagli Pagamento</h2>
      <p>ID Tassa: {{ data.taxId }}</p>
      <p>Importo Pagato: {{ data.amountPaid | currency:'EUR' }}</p>
      <p>Metodo di Pagamento: {{ data.paymentMethod }}</p>
      <p>Data Pagamento: {{ data.paymentDate | date:'medium' }}</p>
      <button mat-button (click)="closeDialog()">Chiudi</button>
    </div>
  `,
  styles: [`
    .custom-dialog {
      background-color: #ffffff;
      border-radius: 8px;
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
      padding: 24px;
      width: 600px;
      max-width: 100%; /* Il testo si adatterà automaticamente alla larghezza della finestra */
      height: 400px;
    }

    h2 {
      font-size: 24px;
      margin-bottom: 16px;
    }

    p {
      font-size: 16px;
      margin-bottom: 8px;
    }

    button {
      background-color: #007bff;
      color: #ffffff;
      border: none;
      border-radius: 4px;
      padding: 8px 16px;
      cursor: pointer;
      transition: background-color 0.3s ease;
    }

    button:hover {
      background-color: #0056b3;
    }
  `]
})
export class PaymentDetailsDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<PaymentDetailsDialogComponent>
  ) {}

  closeDialog() {
    this.dialogRef.close();
  }
}

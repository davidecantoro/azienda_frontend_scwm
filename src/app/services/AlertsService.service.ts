import { Injectable } from '@angular/core';

import { NotificationService } from './NotificationService.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {ALERT_API_CONFIG } from '../config/config';
@Injectable({
  providedIn: 'root'
})
export class AlertsService {
  private _alerts: Array<{ id: string, fillingLevel: number, timestamp: string }> = [];
  private apiBaseUrl = ALERT_API_CONFIG.baseUrl; // URL base dell'API

  constructor(
    private http: HttpClient,
    private notificationService: NotificationService 
  ) {}


  get alerts() {
    return this._alerts;
  }

  addAlert(alert: { id: string, fillingLevel: number, timestamp: string }) {
    const existingAlert = this._alerts.find(a => a.id === alert.id);

    if (alert.fillingLevel < 0.8) {
      if (existingAlert) {
        const index = this._alerts.indexOf(existingAlert);
        if (index > -1) {
          this._alerts.splice(index, 1);
        }
      }
    } else {
      if (existingAlert) {
        existingAlert.fillingLevel = alert.fillingLevel;
        existingAlert.timestamp = alert.timestamp;
      } else {
        this._alerts.push(alert);
      }
    }
  }

  clearAlerts() {
    this._alerts = [];
  }


  cleanContainer(idBin: string): Observable<any> { 
    const token = localStorage.getItem('token');
    if (!token) {
      console.error('Token non trovato');
      return throwError('Token non trovato'); // Ritorna un Observable di errore
    }
  
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
  
    const localISOTime = (new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString()).split('.')[0];
  
    const requestBody = {
      timestamp: localISOTime,
      idBin: idBin,
      fillingLevel: "0.0"
    };
  
    // Qui si ritorna direttamente l'Observable della chiamata HTTP
    return this.http.post(this.apiBaseUrl, requestBody, { headers: headers });
  }

  reportWasteDisposal(disposalData: any): Observable<any> {
    const token = localStorage.getItem('token');
    if (!token) {
      console.error('Token non trovato');
      return throwError('Token non trovato'); // Ritorna un Observable di errore
    }
  
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
  
    return this.http.post(`${this.apiBaseUrl}`, disposalData, { headers });
  }
  
  
}


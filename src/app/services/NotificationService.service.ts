import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
    showToast(message: string): void {
      const toast = document.createElement('div');
      toast.innerText = message;
      toast.style.position = 'fixed';
      toast.style.top = '50%'; // Centra verticalmente
      toast.style.left = '50%'; // Centra orizzontalmente
      toast.style.transform = 'translate(-50%, -50%)'; //
      toast.style.padding = '10px';
      toast.style.backgroundColor = 'rgba(0,0,0,0.7)';
      toast.style.color = 'white';
      toast.style.borderRadius = '5px';
      toast.style.zIndex = '1000';
      toast.style.textAlign = 'center'; // Allinea il testo al centro
      document.body.appendChild(toast);
  
      // Rimuove il toast dopo 3 secondi
      setTimeout(() => document.body.removeChild(toast), 2000);
    }
  }
  

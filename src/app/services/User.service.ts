
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { USER } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 

  constructor(private http: HttpClient) {}

  getUserById(id: string): Observable<any> {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    const url = USER.userById.replace('{id}', id);
    return this.http.get(url, { headers });
  }

  getByRole(role: string, page: number, size: number): Observable<any> {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    
    //const url = `${USER.roleUrl}?role=${encodeURIComponent(role)}&page=${page}&size=${size}`;
    const url = `${USER.roleUrl}${encodeURIComponent(role)}&page=${page}&size=${size}`;
    return this.http.get<any[]>(url, { headers });
  }
  register(user: any): Observable<any> {
   
    const url = USER.registrazione
    return this.http.post(url, user);
  }
  


}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AUTH_API_CONFIG } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = AUTH_API_CONFIG.loginUrl;

  constructor(private http: HttpClient) {}

  login(username: string, password: string) {
    return this.http.post<any>(this.apiUrl, { username, password }).pipe(
      tap(response => {
        localStorage.setItem('token', response.jwt);
        const decodedToken = this.decodeToken(response.jwt);
        if (decodedToken) {
          if (decodedToken.userId) {
            localStorage.setItem('userId', decodedToken.userId);
            console.log('User ID:', decodedToken.userId);
          }
          if (decodedToken.role) {
            localStorage.setItem('role', decodedToken.role);
            console.log('User Role:', decodedToken.role);
          }
        }
      }),
      catchError(error => {
        // Gestisce genericamente gli errori di autenticazione
        let errorMessage = 'Username o password errati';
        
        return throwError(errorMessage);
      })
    );
  }

  private decodeToken(token: string): any {
    try {
      return JSON.parse(atob(token.split('.')[1]));
    } catch (error) {
      console.error('Error decoding token:', error);
      return null;
    }
  }

  isLoggedIn() {
    return !!localStorage.getItem('token');
  }

  getRole() {
    const token = localStorage.getItem('token');
    if (!token) return null;
    const decodedToken = this.decodeToken(token);
    return decodedToken?.role;
  }

  getUsername(): string | null {
    const token = localStorage.getItem('token');
    if (!token) return null;
    const decodedToken = this.decodeToken(token);
    return decodedToken?.username;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('role');
  }
}


import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {
  private selectedContainerIds: string[] = [];

  setSelectedContainerIds(ids: string[]): void {
    this.selectedContainerIds = ids;
  }

  getSelectedContainerIds(): string[] {
    return this.selectedContainerIds;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EMPLOYEE_API_CONFIG } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private apiUrl = EMPLOYEE_API_CONFIG.roleUrl;

  constructor(private http: HttpClient) {}

  getAllEmployees(): Observable<any> {
    // Recupera il token dal local storage
    const token = localStorage.getItem('token');

    // Prepara gli headers della richiesta
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });

    // Effettua la chiamata GET con gli headers
    return this.http.get<any[]>(this.apiUrl, { headers });
  }
}

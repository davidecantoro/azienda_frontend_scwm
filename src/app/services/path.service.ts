import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { PATH_API_CONFIG } from '../config/config';


@Injectable({
  providedIn: 'root'
})
export class PathService {
  private apiUrl = PATH_API_CONFIG.pathUrl;
 

  constructor(private http: HttpClient) {}

  createPath(routeObject: any) {
    const token = localStorage.getItem('token');
    
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

    return this.http.post(this.apiUrl, routeObject, { headers });
  }
  getAllPaths(): Observable<any> {
    const token = localStorage.getItem('token');
    
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.get(this.apiUrl, { headers });
  }
  getPathsByDriverId(driverId: string): Observable<any> {
    const token = localStorage.getItem('token');
    
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    const url = PATH_API_CONFIG.getPathsBydriver.replace('{driverId}', driverId);
    return this.http.get(url, { headers });
  }
  deletePathById(pathId: string): Observable<any> {
    const token = localStorage.getItem('token');
    
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    const url = PATH_API_CONFIG.deletePathById.replace('{pathId}', pathId);
    
    return this.http.delete(url, { headers });
  }

 
  updatePathStatus(pathId: string, status: string): Observable<any> {
    const token = localStorage.getItem('token');
   
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
  
    
    const url = PATH_API_CONFIG.updatePathStatusUrl.replace('{pathId}', pathId);
    
    return this.http.put(url, status, { headers });
  }

}

import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PERFORMANCE } from '../config/config';
import { NOTIFICHE } from '../config/config';
@Injectable({
  providedIn: 'root'
})
export class PerformanceService {
  constructor(private http: HttpClient) {}

  
 

  getLatestPerformanceForCitizens(idCittadini: string[]): Observable<any> {
    const token = localStorage.getItem('token');

    const url = `${PERFORMANCE.performance}/performances/getLatestPerformanceForCitizens`;

    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`); 
    
    return this.http.post(url, { idCittadini }, { headers });
  }
  getPerformanceByCitizen(idCittadino: string): Observable<any> {
    const token = localStorage.getItem('token');

    const url = `${PERFORMANCE.performance}/performances/getLatestPerformanceByCitizen?idCittadino=${idCittadino}`;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    
    return this.http.get(url, { headers });
  }

  creaNotifiche(): Observable<any> {
    

    const url = `${NOTIFICHE.notifiche}/notifiche/creaNotifiche`; 
 
    return this.http.post(url, {}, { headers: this.getAuthHeaders(), responseType: 'text' });
  }
  
  private getAuthHeaders(): HttpHeaders {
    const token = localStorage.getItem('token');

   
    return new HttpHeaders().set('Authorization', `Bearer ${token}`);
  }
  
  getLastYearPerformanceCityHall(): Observable<any> {
    const token = localStorage.getItem('token');
    const url = `${PERFORMANCE.performance}/performances/getLastYearPerformanceCityHall`;

    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    
    return this.http.get(url, { headers });
  }
  getNotificheByCittadino(idCittadino: string): Observable<any> {
    const token = localStorage.getItem('token');
    const url = `${NOTIFICHE.notifiche}/notifiche/getByCittadino/${idCittadino}`; 
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    
    return this.http.get(url, { headers });
  }
  calculatePerformance(data: any): Observable<any> {
    const url = `${PERFORMANCE.performance}/performances/calculatePerformance`;
    return this.http.post(url, data, { headers: this.getAuthHeaders() });
  }

}

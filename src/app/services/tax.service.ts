import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TAX } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class TaxService {
  private apiUrl =`${TAX.tax}/taxes`; 
  
  private apiUrl1 = `${TAX.tax}/payments`;

  constructor(private http: HttpClient) {
  
  }

  calculateTaxes(year: number): Observable<any> {
    const token = localStorage.getItem('token');

    const url = `${this.apiUrl}/calculateTaxes`;
    
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    const body = { year }; // Invia l'anno come oggetto JSON

    return this.http.post<any>(url, body, { headers });
  }
  getTaxesByCitizen(citizenId: string): Observable<any[]> {
    const token = localStorage.getItem('token');

    const url = `${this.apiUrl}/byCitizen/${citizenId}`;
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);

    return this.http.get<any[]>(url, { headers });
  }
  getPaymentDetailsByTaxId(taxId: string): Observable<any> {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    const url = `${this.apiUrl1}/byTaxId/${taxId}`;
    
    return this.http.get<any>(url, { headers });
  }
 
  makePayment(paymentDetails: any): Observable<any> {
    const url = `${this.apiUrl1}/makePayment`;
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    
    return this.http.post<any>(url, paymentDetails, { headers });
  }
}

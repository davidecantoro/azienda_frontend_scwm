import { Injectable } from '@angular/core';
import {User} from "../models/user";
import {Observable, of} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UsersService {


  //don't do it !!!! Usa API Gateway e EC2
  users:User[] = []

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':''
    })
  }

  constructor(private http:HttpClient) {

    //TODO il token va preso mediante chiamata API diretta (authentication)
    localStorage.setItem('currentUser', JSON.stringify({token:'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtaWNoZWxlcyIsImlhdCI6MTY5MjQ3MzUzMiwiZXhwIjoxNjkyNTA5NTMyfQ.aZXGBeGdPhsU7Xmvc8VDtX47UUe0j2c6NABO3NwjKG0'}))


    
  }

  

  createUser(user:User) {
    this.users.unshift(user);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VEHICLE } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
 
  private apiUrl = VEHICLE.getVehiclesOffRoute


  constructor(private http: HttpClient) {}

  getVehiclesOffRoute(): Observable<any> {
    const token = localStorage.getItem('token');

    
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

    return this.http.get(this.apiUrl, { headers });
  }
  getVehicleById(id: string): Observable<any> {
    const token = localStorage.getItem('token');

    
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    const url = VEHICLE.getVehiclesById.replace('{id}', id);
    return this.http.get(url, { headers });
  }




  updateVehicleStatus(id: string, status: string): Observable<any> {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    const body = status; 
    const url = VEHICLE.updateVehicleStatus.replace('{id}', id);
  
    return this.http.put(url, body, { headers });
  }
}

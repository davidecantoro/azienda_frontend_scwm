import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { WasteContainerModel } from '../models/waste-container-model.interface';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { WASTCONTAINER } from '../config/config';
export interface MeasureWasteBinDTO {
  id: string;
  timestamp: Date;
  idBin: string;
  fillingLevel: number;
}

@Injectable({
  providedIn: 'root'
})
export class WasteContainerService {
  private apiUrl = WASTCONTAINER.apiUrl;
  private apiUrl1 = WASTCONTAINER.getModels
  

  constructor(private http: HttpClient) {}

  createWasteContainer(containerData: any): Observable<WasteContainerModel> {
    const token = localStorage.getItem('token');

    
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);

    return this.http.post<WasteContainerModel>(this.apiUrl, containerData, { headers });
  }

  getAllWasteContainers(): Observable<any> {
    const token = localStorage.getItem('token');

    
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    return this.http.get(this.apiUrl, { headers });
  }

  getContainerDetails(id: string): Observable<any> {
    const token = localStorage.getItem('token');

  
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    const url = WASTCONTAINER.getContainerDetails.replace('{id}',id); 
    return this.http.get(url, { headers });
  }

  


  getLatestFillingLevels(ids: string[]): Observable<MeasureWasteBinDTO[]> {
    const token = localStorage.getItem('token');

   
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    
    return this.http.post<MeasureWasteBinDTO[]>(WASTCONTAINER.getLatestFillingLevels, ids, { headers });
  }
  
 


  getContainersByIds(ids: string[]): Observable<any> {
    const token = localStorage.getItem('token');

    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);

    // Usa http.post e passa l'array di ID nel corpo della richiesta
    return this.http.post(WASTCONTAINER.getContainerByIds, ids, { headers });
}

  sortWasteContainers(containerIds: string[]): Observable<string[]> {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);

    return this.http.post<string[]>(WASTCONTAINER.sortWasteContainers, containerIds, { headers });
  }

  getAllWasteContainerModels(): Observable<WasteContainerModel[]> {
    const token = localStorage.getItem('token');

    
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    return this.http.get<WasteContainerModel[]>(`${this.apiUrl1}`, { headers });
  }
  updateWasteContainerLocation(containerData: any): Observable<string> {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);

    return this.http.put(WASTCONTAINER.updateWasteContainerLocation, containerData, {
      headers,
      responseType: 'text'
    }).pipe(
      catchError(error => {
        console.error('Error updating waste container:', error);
        return throwError(error); 
      })
    );
  }
  
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WASTCONTAINER } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class WasteContainerModelService {
  

  constructor(private http: HttpClient) {}

  

  getModelContainerDetails(id: string): Observable<any> {
    const url = WASTCONTAINER.getModelById.replace('{id}',id)
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    return this.http.get(url, { headers });
  }
}

import { Injectable } from '@angular/core';
import { Stomp } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import * as CryptoJS from 'crypto-js';
import { WEBSOCKET } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private stompClient: any;
  private SECRET_KEY = 'bZ4t7#RcQ!p9W0yJ';  
  private INIT_VECTOR = CryptoJS.enc.Utf8.parse('1234567890123456');  // Initialization Vector
  private apiUrl = WEBSOCKET.websocket

  constructor() { }

  connect(onConnectedCallback: () => void) {
    const socket = new SockJS(this.apiUrl);
    this.stompClient = Stomp.over(socket);

    this.stompClient.connect({}, () => {
        console.log('Connesso con successo!');
        
  
       

       
        onConnectedCallback();
    }, (error: any) => {
        console.error('Errore di connessione:', error);
    });
}



  private encrypt(data: string): string {
    const key = CryptoJS.enc.Utf8.parse(this.SECRET_KEY);
    const encrypted = CryptoJS.AES.encrypt(data, key, {
        iv: this.INIT_VECTOR,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
  }

  private decrypt(encryptedData: string): string {
    const key = CryptoJS.enc.Utf8.parse(this.SECRET_KEY);
    const decrypted = CryptoJS.AES.decrypt(encryptedData, key, {
        iv: this.INIT_VECTOR,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  subscribeToTopic(topic: string, callback: any) {
    if (this.stompClient && this.stompClient.connected) {
      this.stompClient.subscribe(topic, callback);
    } else {
      console.error('Errore: tentativo di sottoscrizione senza connessione stabilita.');
    }
  }

  disconnect() {
    if (this.stompClient) {
      this.stompClient.disconnect(() => {
        console.log('Disconnesso dal WebSocket');
      });
    }
  }
}
